﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BeeMove : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}

    public float speed = 4.0f;
    public float turnSpeed = 180.0f;  // degrees per second
    public Transform target;
    public Transform player1;
    public Transform player2;
    private Vector2 heading = Vector2.right;


    // Update is called once per frame
    void Update () {
        // get the vector from the bee to the target 
        Vector2 direction = target.position - transform.position;

        // calculate how much to turn per frame
        float angle = turnSpeed * Time.deltaTime;

        //move to closer player
        

        // turn left or right
        if (direction.IsOnLeft(heading))
        {
            // target on left, rotate anticlockwise
            heading = heading.Rotate(angle);
        }
        else
        {
            // target on right, rotate clockwise
            heading = heading.Rotate(-angle);
        }

        transform.Translate(heading * speed * Time.deltaTime);


        //find closer player
        if (Vector3.Distance(player1.position, transform.position) < Vector3.Distance(player2.position , transform.position))
        {
            target = player1;

        }
        if (Vector3.Distance(player1.position, transform.position) > Vector3.Distance(player2.position, transform.position))
        {
            target = player2;

        }
    }

    void OnDrawGizmos()
    {
        // draw heading vector in red
        Gizmos.color = Color.red;
        Gizmos.DrawRay(transform.position, heading);

        // draw target vector in yellow
        Gizmos.color = Color.yellow;
        Vector2 direction = target.position - transform.position;
        Gizmos.DrawRay(transform.position, direction);
    }

}
